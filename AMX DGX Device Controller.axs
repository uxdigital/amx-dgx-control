MODULE_NAME='AMX DGX Device Controller' (DEV controller, DEV switcher)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 10/06/2014  AT: 10:44:46        *)
(***********************************************************)
(*******************************************************************************)
(*                                                                             *)
(*                   � Control Designs Software Ltd (2012)                     *)
(*                         www.controldesigns.co.uk                            *)
(*                                                                             *)
(*      Tel: +44 (0)1753 208 490     Email: support@controldesigns.co.uk       *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*                       AMX DGX Switcher Controller                           *)
(*                                                                             *)
(*            Written by Mike Jobson (Control Designs Software Ltd)            *)
(*                                                                             *)
(** REVISION HISTORY ***********************************************************)
(*                                                                             *)
(*  Please see README.md included with this project                            *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*  Permission is hereby granted, free of charge, to any person obtaining a    *)
(*  copy of this software and associated documentation files (the "Software"), *)
(*  to deal in the Software without restriction, including without limitation  *)
(*  the rights to use, copy, modify, merge, publish, distribute, sublicense,   *)
(*  and/or sell copies of the Software, and to permit persons to whom the      *)
(*  Software is furnished to do so, subject to the following conditions:       *)
(*                                                                             *)
(*  The above copyright notice and this permission notice shall be included in *)
(*  all copies or substantial portions of the Software.                        *)
(*                                                                             *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *)
(*  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *)
(*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *)
(*  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *)
(*  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT  *)
(*  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR   *)
(*  THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                 *)
(*                                                                             *)
(*******************************************************************************)


(*******************************************************************************)
(*  IMPORT CORE LIBRARY HERE                                                   *)
(*  This is includes generic functions and code which can be re-used in main   *)
(*  and other modules. Also includes 'SNAPI' and some add-on functions.        *)
(*******************************************************************************)
#DEFINE CORE_LIBRARY
//#DEFINE DEBUG
#INCLUDE 'Core Library'
#INCLUDE 'Core Debug'
#INCLUDE 'SNAPI'

DEFINE_CONSTANT

INTEGER DGX_SIZE = 16

INTEGER NAME_MAX_SIZE = 50

DEFINE_TYPE

STRUCT _SW_INPUT {
    INTEGER inputVal
    CHAR name[NAME_MAX_SIZE]
}

STRUCT _SW_OUTPUT {
    INTEGER outputVal
    CHAR name[NAME_MAX_SIZE]
    INTEGER currentInput
}

DEFINE_VARIABLE

_SW_INPUT inputs[DGX_SIZE]
_SW_OUTPUT outputs[DGX_SIZE]

DEFINE_FUNCTION InitData() {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(inputs); n ++) {
	inputs[n].inputVal = n
	inputs[n].name = ''
    }
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(outputs); n ++) {
	outputs[n].outputVal = n
	outputs[n].name = ''
	outputs[n].currentInput = 0
    }
}

DEFINE_FUNCTION INTEGER InputIndex(INTEGER input) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(inputs); n ++) {
	if(inputs[n].inputVal == input) return n
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER OutputIndex(INTEGER output) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(outputs); n ++) {
	if(outputs[n].outputVal == output) return n
    }
    
    return 0
}

DEFINE_FUNCTION OutputNameSet(INTEGER output, CHAR name[]) {
    STACK_VAR INTEGER index
    
    index = OutputIndex(output)
    
    if(index) {
	outputs[index].name = name
	SEND_COMMAND controller, "'OUTPUT_NAME-', itoa(output), ',', OutputName(output)"
	RequestOutputStatus(output)
    }
}

DEFINE_FUNCTION InputNameSet(INTEGER input, CHAR name[]) {
    STACK_VAR INTEGER index
    
    index = InputIndex(input)
    
    if(index) {
	inputs[index].name = name
	SEND_COMMAND controller, "'INPUT_NAME-', itoa(input), ',', InputName(input)"
    }
}

DEFINE_FUNCTION CHAR[NAME_MAX_SIZE] OutputName(INTEGER output) {
    STACK_VAR INTEGER index
    
    index = OutputIndex(output)
    
    if(index) {
	return outputs[index].name
    }
    
    return ''
}

DEFINE_FUNCTION CHAR[NAME_MAX_SIZE] InputName(INTEGER input) {
    STACK_VAR INTEGER index
    
    index = InputIndex(input)
    
    if(index) {
	return inputs[index].name
    }
    
    return ''
}

DEFINE_FUNCTION SendOutputStatus(INTEGER output) {
    STACK_VAR INTEGER input
    
    input = OutputInput(output)
    
    SEND_COMMAND controller, "'OUTPUT_STATUS-', ItoA(output), ',', OutputName(output), ',', ItoA(input), ',', InputName(input)"
}

DEFINE_FUNCTION OutputInputSet(INTEGER output, INTEGER input) {
    STACK_VAR INTEGER index
    
    index = OutputIndex(output)
    
    if(index) {
	outputs[index].currentInput = input
	SendOutputStatus(output)
    }
}

DEFINE_FUNCTION INTEGER OutputInput(INTEGER output) {
    STACK_VAR INTEGER index
    
    index = OutputIndex(output)
    
    if(index) {
	return outputs[index].currentInput
    }
    
    return 0
}

DEFINE_FUNCTION ConnectInputToOutput(INTEGER input, INTEGER output) {
    if(output && input) {
	SEND_COMMAND switcher, "'CI', itoa(input), 'O', itoa(output), 'T'"
	RequestOutputStatus(output)
    }
}

DEFINE_FUNCTION DisconnectOutput(INTEGER output) {
    if(output) {
	SEND_COMMAND switcher, "'DO', itoa(output), 'T'"
	RequestOutputStatus(output)
    }
}

DEFINE_FUNCTION DisconnectInput(INTEGER input) {
    if(input) {
	SEND_COMMAND switcher, "'DI', itoa(input), 'T'"
    }
}

DEFINE_FUNCTION RequestOutputStatus(INTEGER output) {
    if(output) {
	SEND_COMMAND switcher, "'SL0O', itoa(output), 'T'"
    }
}

DEFINE_EVENT

DATA_EVENT[controller] {
    ONLINE: {
	InitData()
	ON[data.device, DATA_INITIALIZED]
	ON[data.device, POWER_FB]
    }
    COMMAND: {
	STACK_VAR _SNAPI_DATA snapi
	
	SNAPI_InitDataFromDevice(snapi, data.device, data.text)
	//SNAPI_Debug(snapi)
	
	switch(snapi.cmd) {
	    case 'ROUTE' : {
		ConnectInputToOutput(atoi(snapi.param[1]), atoi(snapi.param[2]))
	    }
	    case 'OUTPUT_CLEAR' : {
		DisconnectOutput(atoi(snapi.param[1]))
	    }
	    case 'INPUT_CLEAR' : {
		DisconnectInput(atoi(snapi.param[1]))
	    }
	    case 'INPUT_NAME_SET': {
		InputNameSet(atoi(snapi.param[1]), snapi.param[2])
	    }
	    case 'INPUT_NAME?': {
		STACK_VAR INTEGER input
		
		input = atoi(snapi.param[1])
		
		if(InputIndex(input)) {
		    SEND_COMMAND data.device, "'INPUT_NAME-', itoa(input), ',', InputName(input)"
		}
	    }
	    case 'OUTPUT_NAME_SET': {
		OutputNameSet(atoi(snapi.param[1]), snapi.param[2])
	    }
	    case 'OUTPUT_NAME?': {
		STACK_VAR INTEGER output
		
		output = atoi(snapi.param[1])
		
		if(OutputIndex(output)) {
		    SEND_COMMAND data.device, "'OUTPUT_NAME-', itoa(output), ',', OutputName(output)"
		}
	    }
	    case 'OUTPUT_STATUS?': {
		STACK_VAR INTEGER n
		
		n = atoi(snapi.param[1])
		
		if(!n) {
		    for(n = 1; n <= DGX_SIZE; n ++) {
			RequestOutputStatus(n)
		    }
		} else {
		    RequestOutputStatus(n)
		}
	    }
	    default: {}
	}
    }
}

DATA_EVENT[switcher] {
    ONLINE: {
	ON[controller, DEVICE_COMMUNICATING]
    }
    COMMAND: {
	STACK_VAR INTEGER input
	STACK_VAR INTEGER output
	STACK_VAR CHAR commandFromDevice[255]
	
	commandFromDevice = data.text
	
	if(FIND_STRING(commandFromDevice, 'SL', 1) && FIND_STRING(commandFromDevice, 'O', 1)) {
	    output = AtoI(StringFromTwoDelimiters(commandFromDevice, 'O', 'T', 1))
	    input = AtoI(StringFromTwoDelimiters(commandFromDevice, '(', ')', 1))
	    
	    OutputInputSet(output, input)
	} if(FIND_STRING(commandFromDevice, 'CI', 1) && FIND_STRING(commandFromDevice, 'O', 1)) {
	    input = AtoI(StringFromTwoDelimiters(commandFromDevice, 'CI', 'O', 1))
	    output = AtoI(StringFromTwoDelimiters(commandFromDevice, 'O', 'T', 1))
	    
	    OutputInputSet(output, input)
	} if(FIND_STRING(commandFromDevice, 'DO', 1) && FIND_STRING(commandFromDevice, 'O', 1)) {
	    output = AtoI(StringFromTwoDelimiters(commandFromDevice, 'O', 'T', 1))
	    
	    OutputInputSet(output, 0)
	}
    }
}