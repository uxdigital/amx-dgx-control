MODULE_NAME='AMX DGX UI Controller' (DEV controller, DEV uiDevices[])
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 12/16/2014  AT: 23:11:33        *)
(***********************************************************)
(*******************************************************************************)
(*                                                                             *)
(*                   � Control Designs Software Ltd (2012)                     *)
(*                         www.controldesigns.co.uk                            *)
(*                                                                             *)
(*      Tel: +44 (0)1753 208 490     Email: support@controldesigns.co.uk       *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*                          AMX DGX UI Controller                              *)
(*                                                                             *)
(*            Written by Mike Jobson (Control Designs Software Ltd)            *)
(*                                                                             *)
(** REVISION HISTORY ***********************************************************)
(*                                                                             *)
(*  Please see README.md included with this project                            *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*  Permission is hereby granted, free of charge, to any person obtaining a    *)
(*  copy of this software and associated documentation files (the "Software"), *)
(*  to deal in the Software without restriction, including without limitation  *)
(*  the rights to use, copy, modify, merge, publish, distribute, sublicense,   *)
(*  and/or sell copies of the Software, and to permit persons to whom the      *)
(*  Software is furnished to do so, subject to the following conditions:       *)
(*                                                                             *)
(*  The above copyright notice and this permission notice shall be included in *)
(*  all copies or substantial portions of the Software.                        *)
(*                                                                             *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *)
(*  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *)
(*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *)
(*  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *)
(*  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT  *)
(*  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR   *)
(*  THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                 *)
(*                                                                             *)
(*******************************************************************************)



(*******************************************************************************)
(*  IMPORT CORE LIBRARY HERE                                                   *)
(*  This is includes generic functions and code which can be re-used in main   *)
(*  and other modules. Also includes 'SNAPI' and some add-on functions.        *)
(*******************************************************************************)
#DEFINE CORE_LIBRARY
//#DEFINE DEBUG
#INCLUDE 'Core Library'
#INCLUDE 'Core Debug'
#INCLUDE 'SNAPI'

DEFINE_CONSTANT

INTEGER DGX_SIZE = 16

DEFINE_VARIABLE

INTEGER outputStatus[DGX_SIZE]
CHAR inputName[DGX_SIZE][50]
CHAR outputName[DGX_SIZE][50]

(***********************************************************)
(*  UI Setup                                               *)
(***********************************************************)

DEFINE_CONSTANT
CHAR UI_DEVICES[] = 'UI_MAIN'

INTEGER UI_MAX_DEVICES = 2

#DEFINE UI_KIT
#INCLUDE 'UI Kit API'

DEFINE_FUNCTION UserInterfacesShouldRegister() {
    STACK_VAR INTEGER uiIndex
    
    for(uiIndex = 1; uiIndex <= MAX_LENGTH_ARRAY(uiDevices); uiIndex ++) {
	UIRegisterDevice("'DGX_UI_', itoa(uiIndex)", "'AMX DGX UI ', itoa(uiIndex)", UI_DEVICES, uiDevices[uiIndex])
    }
}

DEFINE_FUNCTION UIDidComeOnline(CHAR uiDeviceKey[]) {
    UIUpdate(uiDeviceKey)
}

DEFINE_FUNCTION UIDidGoOffline(CHAR uiDeviceKey[]) {
    
}

DEFINE_FUNCTION UICommandEvent(CHAR uiDeviceKey[], CHAR text[]) {
    STACK_VAR _SNAPI_DATA snapi
    
    SNAPI_InitDataFromDevice(snapi, uiDevices[UIGetDeviceIndexFromKey(uiDeviceKey)], text)
    //SNAPI_Debug(snapi)
    
    switch(snapi.cmd) {
	default: {}
    }
}

DEFINE_FUNCTION UIStringEvent(CHAR uiDeviceKey[], CHAR text[]) {
    
}

DEFINE_CONSTANT

CHAR UIPROP_INPUT_SELECTED[] = 'UIPROP_INPUT_SELECTED'

DEFINE_FUNCTION UserInterfaceVarsShouldRegister() {
    // Register any variables used for the device
    
    UIVarRegister(UI_DEVICES, UIPROP_INPUT_SELECTED, '1')
}

DEFINE_FUNCTION UserInterfaceHasRegistered(CHAR uiDeviceKey[]) {
    // Anything you may want to call after the device has been reistered
    
}


(***********************************************************)
(*  UI Interaction Callbacks                               *)
(*  Use for starting / ending timelines and timeouts       *)
(***********************************************************)

DEFINE_FUNCTION UIInteractionHasStarted(CHAR uiDeviceKey[]) {
    DebugSendStringToConsole("'UI Interaction Start - Device: ', uiDeviceKey")
}

DEFINE_FUNCTION UIInteractionHasEnded(CHAR uiDeviceKey[]) {
    DebugSendStringToConsole("'UI Interaction End - Device: ', uiDeviceKey")
}

//Counts seconds after complete button events. Return TRUE to kill the timeline if you are done.
DEFINE_FUNCTION INTEGER UIInteractionPostCount(CHAR uiDeviceKey[], INTEGER seconds) {
    DebugSendStringToConsole("'UI Interaction Post Count - Device: ', uiDeviceKey, ', Count: ', itoa(seconds)")
    
    switch(seconds) {
	case 5: {
	    return TRUE
	}	
    }
    
    return FALSE
}


(***********************************************************)
(*  MAIN BUTTON EVENT CALLBACKS                            *)
(***********************************************************)

DEFINE_CONSTANT

CHAR BK_GROUP_INPUTS[] = 'BK_GROUP_INPUTS'
CHAR BK_GROUP_OUTPUTS[] = 'BK_GROUP_OUTPUTS'

DEFINE_FUNCTION UserInterfaceButtonsShouldRegister() {
    STACK_VAR INTEGER btnIndex
    
    for(btnIndex = 1; btnIndex <= DGX_SIZE; btnIndex ++) {
	UIRegisterButton("'INPUT_BTN_', itoa(btnIndex)", BK_GROUP_INPUTS, '', btnIndex + 10, FALSE)
    }
    
    for(btnIndex = 1; btnIndex <= DGX_SIZE; btnIndex ++) {
	UIRegisterButton("'OUTPUT_BTN_', itoa(btnIndex)", BK_GROUP_OUTPUTS, '', btnIndex + 50, TRUE)
    }
}

DEFINE_FUNCTION UIButtonEvent(CHAR uiDeviceKey[], CHAR btnKey[], INTEGER eventType) {
    switch(eventType) {
	case UI_BTN_EVENT_TYPE_TAP: {
	    DebugSendStringToConsole("'Button [TAP] ', UIDeviceName(uiDeviceKey), ', ', UIButtonName(btnKey), $20, $22, btnKey, $22")
	    
	    switch(UIButtonGroupKey(btnKey)) {
		case BK_GROUP_INPUTS: {
		    STACK_VAR INTEGER input
		    
		    input = UIButtonIndexInGroup(btnKey)
		    SelectInput(uiDeviceKey, input)
		}
		case BK_GROUP_OUTPUTS: {
		    STACK_VAR INTEGER output
		    STACK_VAR INTEGER input
		    
		    output = UIButtonIndexInGroup(btnKey)
		    input = atoi(UIGetVarValue(uiDeviceKey, UIPROP_INPUT_SELECTED))
		    
		    DebugSendStringToConsole("'DGX UI[', uiDeviceKey, '] Toggle Output: ', itoa(output)")
		    
		    OutputToggleInput(output, input)
		}
		default: {}
	    }
	}
	case UI_BTN_EVENT_TYPE_HOLD: {
	    DebugSendStringToConsole("'Button [HOLD] ', UIDeviceName(uiDeviceKey), ', ', UIButtonName(btnKey), $20, $22, btnKey, $22")
	    
	    switch(UIButtonGroupKey(btnKey)) {
		case BK_GROUP_INPUTS: {
		    STACK_VAR INTEGER input
		    
		    input = UIButtonIndexInGroup(btnKey)
		    InputClearOutputs(input)
		}
		case BK_GROUP_OUTPUTS: {
		    STACK_VAR INTEGER output
		    
		    output = UIButtonIndexInGroup(btnKey)
		    
		    if(outputStatus[output]) {
			SelectInput(uiDeviceKey, outputStatus[output])
		    }
		}
		default: {}
	    }
	}
	default: {}
    }
}

DEFINE_FUNCTION INTEGER UIButtonFeedback(CHAR UIDeviceKey[], CHAR btnKey[]) {
    
    switch(UIButtonGroupKey(btnKey)) {
	case BK_GROUP_INPUTS: {
	    if(UIButtonIndexInGroup(btnKey) == atoi(UIGetVarValue(UIDeviceKey, UIPROP_INPUT_SELECTED))) return TRUE
	}
	case BK_GROUP_OUTPUTS: {
	    STACK_VAR INTEGER input
	    STACK_VAR INTEGER output
	    
	    input = atoi(UIGetVarValue(UIDeviceKey, UIPROP_INPUT_SELECTED))
	    output = UIButtonIndexInGroup(btnKey)
	    
	    if(output) {
		if(outputStatus[output] == input) return TRUE
	    }
	}
	default: return FALSE
    }
    
    return FALSE
}



(***********************************************************)
(*  MAIN LOGIC                                             *)
(***********************************************************)

DEFINE_FUNCTION SelectInput(CHAR uiDeviceKey[], INTEGER input) {
    STACK_VAR INTEGER currentInput
    
    DebugSendStringToConsole("'DGX UI[', uiDeviceKey, '] Selected Input: ', itoa(input)")
    
    currentInput = atoi(UIGetVarValue(uiDeviceKey, UIPROP_INPUT_SELECTED))
    [uiDevices[UIGetDeviceIndexFromKey(uiDeviceKey)], 10 + currentInput] = FALSE
    
    UISetVarValue(uiDeviceKey, UIPROP_INPUT_SELECTED, itoa(input))
}

DEFINE_FUNCTION OutputToggleInput(INTEGER output, INTEGER input) {
    if(output && input && outputStatus[output] == input) {
	SEND_COMMAND controller, "'OUTPUT_CLEAR-', itoa(output)"
    } else if(output && input) {
	SEND_COMMAND controller, "'ROUTE-', itoa(input), ',', itoa(output)"
    } else if(output) {
	SEND_COMMAND controller, "'OUTPUT_CLEAR-', itoa(output)"
    }
}

DEFINE_FUNCTION InputClearOutputs(INTEGER input) {
    SEND_COMMAND controller, "'INPUT_CLEAR-', itoa(input)"
}

DEFINE_FUNCTION UIUpdate(CHAR uiDeviceKey[]) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= DGX_SIZE; n ++) {
	if(LENGTH_STRING(inputName[n])) {
	    UIText(uiDeviceKey, 10 + n, UI_STATE_ALL, inputName[n])
	} else {
	    UIText(uiDeviceKey, 10 + n, UI_STATE_ALL, "'Input ', itoa(n)")
	}
	UIButtonShow(uiDeviceKey, 10 + n)
    }
    
    for(n = 1; n <= DGX_SIZE; n ++) {
	if(LENGTH_STRING(outputName[n])) {
	    UIText(uiDeviceKey, 50 + n, UI_STATE_ALL, outputName[n])
	} else {
	    UIText(uiDeviceKey, 50 + n, UI_STATE_ALL, "'Output ', itoa(n)")
	}
	UIButtonShow(uiDeviceKey, 50 + n)
    }
}

DEFINE_EVENT

DATA_EVENT[controller] {
    COMMAND: {
	STACK_VAR _SNAPI_DATA snapi
	
	SNAPI_InitDataFromDevice(snapi, data.device, data.text)
	//SNAPI_Debug(snapi)
	
	switch(snapi.cmd) {
	    case 'OUTPUT_STATUS': {
		STACK_VAR INTEGER output
		STACK_VAR INTEGER input
		
		output = atoi(snapi.param[1])
		input = atoi(snapi.param[3])
		
		if(output) {
		    outputStatus[output] = input
		}
	    }
	    case 'INPUT_NAME': {
		STACK_VAR CHAR name[50]
		STACK_VAR INTEGER value
		
		value = atoi(snapi.param[1])
		
		if(value) {
		    inputName[value] = snapi.param[2]
		}
	    }
	    case 'OUTPUT_NAME': {
		STACK_VAR CHAR name[50]
		STACK_VAR INTEGER value
		
		value = atoi(snapi.param[1])
		
		if(value) {
		    outputName[value] = snapi.param[2]
		}
	    }
	    case 'UI_INIT': {
		STACK_VAR CHAR uiDeviceKey[UI_KEY_MAX_LENGTH]
		
		uiDeviceKey = UIGetKeyForIndex(atoi(snapi.param[1]))
		
		DebugSendStringToConsole("'DGX Init UI[', uiDeviceKey, ']'")
		
		if(LENGTH_STRING(uiDeviceKey)) {
		    UIUpdate(uiDeviceKey)
		}
	    }
	}
    }
}